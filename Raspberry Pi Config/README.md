# Raspberry Pi Setup

## General setup
Install (Astroberry)[https://astroberry.io]
```
sudo apt update
sudo apt upgrade -y
sudo apt install -y vim git mc
```

## Setup Atmel programming 
```
sudo apt-get install -y avrdude
run configure_hardware.sh in the Raspberry Pi Config Folder
```

## Setup platform.io
```
sudo apt install -y python3-venv
python -m venv venv
. venv/bin/activate
pip install platformio
```
