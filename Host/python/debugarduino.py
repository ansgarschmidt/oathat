import oathat_pb2
import serial
import struct
import time

inhib = time.time()

with serial.Serial("/dev/ttyUSB0", 115200, timeout = 10) as ser:
    while(1):
        if ser.inWaiting() > 0:
            print(ser.read().decode("utf-8"), end = '')

        if time.time() - inhib > 10:
            print("X")
            message                            = oathat_pb2.Hat2Heater()
            message.setting.set_temp_sensor    = 5
            message.setting.set_temp_telescope = 6
            message.setting.set_temp_guidance  = 8
            data                               = message.SerializeToString()
            ser.write(b'\x45\x4C\x4F\x4E')
            ser.write(struct.pack('!h', len(data)))
            ser.write(data)
            inhib = time.time()
