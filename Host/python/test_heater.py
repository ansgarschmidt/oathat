import oathat_pb2
import serial
import struct

WAITING_FOR_E      = 0
WAITING_FOR_L      = 1
WAITING_FOR_O      = 2
WAITING_FOR_N      = 3
WAITING_FOR_SIZE_H = 4
WAITING_FOR_SIZE_L = 5
WAITING_FOR_DATA   = 6

def setSettings(ser, sensor, telescope, guidance):
    message                            = oathat_pb2.Hat2Heater()
    message.setting.set_temp_sensor    = sensor
    message.setting.set_temp_telescope = telescope
    message.setting.set_temp_guidance  = guidance
    data                               = message.SerializeToString()
    ser.write(b'\x45\x4C\x4F\x4E')
    ser.write(struct.pack('!h', len(data)))
    ser.write(data)

def pb2csv(bp):
    #system
    print(f"Heater:{bp.system.version_heater:2d}, Sensor:{bp.system.temp_sensor:10.7f}, Pletier:{bp.system.temp_peltier:10.7f}, Cooling{bp.system.temp_cooling:10.7f}, {bp.system.temp_telescope:10.7f}, {bp.system.temp_guidance:10.7f}")
    #state
    #print(f"{bp.state.percentage_peltier_1:3d}, {bp.state.percentage_peltier_2:3d}, {bp.state.percentage_cooling_fan:3d}, {bp.state.sensor_state}, {bp.state.percentage_telescope:3d}, {bp.state.percentage_guidance:3d}")
    #environment
    #print(f"{bp.environment.temp_air_telescope:10.7f}")
    #settings
    # print(f"{bp.setting.set_temp_sensor:10.7f}, {bp.setting.set_temp_telescope:10.7f}, {bp.setting.set_temp_guidance:10.7f}")

def decodeAnswer(ser, amount):
    data      = None
    data_size = 0
    state     = WAITING_FOR_E
    counter   = 0

    while True:
        c = ser.read()

        if state == WAITING_FOR_E:
            if c == b'E':
                state = WAITING_FOR_L
                continue

        if state == WAITING_FOR_L:
            if c == b'L':
                state = WAITING_FOR_O
            else:
                state = WAITING_FOR_E
            continue

        if state == WAITING_FOR_O:
            if c == b'O':
                state = WAITING_FOR_N
            else:
                state = WAITING_FOR_E
            continue

        if state == WAITING_FOR_N:
            if c == b'N':
                state = WAITING_FOR_SIZE_H
            else:
                state = WAITING_FOR_E
            continue

        if state == WAITING_FOR_SIZE_H:
            data = c
            state = WAITING_FOR_SIZE_L
            continue

        if state == WAITING_FOR_SIZE_L:
            data        += c
            data_size    = struct.unpack('!h', data)[0]
            data         = ser.read(data_size)
            test_message = oathat_pb2.Heater2Hat()
            test_message.ParseFromString(data)
            pb2csv(test_message)
            state = WAITING_FOR_E

            counter += 1

            if counter == amount:
                return

            continue

with serial.Serial("/dev/ttyUSB0", 115200, timeout = 10) as ser:
    decodeAnswer(ser, 5)
    setSettings(ser, 12.23, 15.55, 19.83)
    decodeAnswer(ser, 5000)
