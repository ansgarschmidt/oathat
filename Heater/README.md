# setup
```bash
sudo apt install protobuf-compiler
python3 -m venv venv
. venv/bin/activate
pip install platformio
pio run
```