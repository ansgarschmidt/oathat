#pragma once

#define PIN_RX             0
#define PIN_TX             1
#define PIN_IN_BUTTON      2
#define PIN_OUT_PELTIER_1  3
#define PIN_OUT_STATUS_1   4
#define PIN_OUT_PELTIER_2  5
#define PIN_OUT_GUIDANCE   6
#define PIN_OUT_STATUS_2   7
#define PIN_OUT_STATUS_3   8
#define PIN_OUT_TELESCOPE  9
#define PIN_OUT_FAN       10
#define PIN_MOSI          11
#define PIN_MISO          12
#define PIN_SCK           13

#define PIN_IN_TELESCOPE  A0
#define PIN_IN_GUIDANCE   A1
#define PIN_IN_AIR        A2
#define PIN_IN_SENSOR     A3
#define PIN_IN_PELTIER    A4
#define PIN_IN_COOLING    A5
