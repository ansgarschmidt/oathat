#pragma once

#define SENSOR_LOW_PASS_FILTER_NEW 0.1
#define SENSOR_LOW_PASS_FILTER_OLD (1 - SENSOR_LOW_PASS_FILTER_NEW)

void sensor_init   (void);
void sensor_measure(void);
