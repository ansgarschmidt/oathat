#include <Arduino.h>

#include "main.h"
#include "heater.h"
#include "uplink.h"
#include "pinout.h"

uint32_t heater_last_millis = 0;

// Local
void heater_calculate (void);
void heater_set_output(void);

void heater_init(void){
    pinMode(PIN_OUT_PELTIER_1, OUTPUT);
    pinMode(PIN_OUT_PELTIER_2, OUTPUT);
    pinMode(PIN_OUT_FAN,       OUTPUT);
    pinMode(PIN_OUT_TELESCOPE, OUTPUT);
    pinMode(PIN_OUT_GUIDANCE,  OUTPUT);

    digitalWrite(PIN_OUT_PELTIER_1, LOW);
    digitalWrite(PIN_OUT_PELTIER_2, LOW);
    digitalWrite(PIN_OUT_FAN,       LOW);
    digitalWrite(PIN_OUT_TELESCOPE, LOW);
    digitalWrite(PIN_OUT_GUIDANCE,  LOW);
}

void heater_start_ramp(void){
    heater_last_millis = 1;
}

void heater_cooling_ramp(void){
    if (heater_last_millis > 0){
        if(millis() - heater_last_millis > HEATER_WAIT_TIME_BETWEEN_STEP){
            heater_last_millis = millis();
            if (uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_OFF     ||
                uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_COOLING ||
                uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_LEVELING){
                uplink_out_message.setting.set_temp_guidance  = uplink_out_message.environment.temp_air_telescope + HEATER_TEMP_DELTA_HEATING;
                uplink_out_message.setting.set_temp_telescope = uplink_out_message.environment.temp_air_telescope + HEATER_TEMP_DELTA_HEATING;
                if(uplink_out_message.setting.set_temp_sensor > HEATER_RAMP_LOW){
                    uplink_out_message.setting.set_temp_sensor--;
                } else {
                    heater_last_millis = 0;
                }
            } else {
                uplink_out_message.setting.set_temp_guidance  = uplink_out_message.environment.temp_air_telescope - HEATER_TEMP_DELTA_HEATING;
                uplink_out_message.setting.set_temp_telescope = uplink_out_message.environment.temp_air_telescope - HEATER_TEMP_DELTA_HEATING;
                if(uplink_out_message.setting.set_temp_sensor < HEATER_RAMP_HIGH){
                    uplink_out_message.setting.set_temp_sensor++;
                } else {
                    heater_last_millis = 0;
                }
            }
        }
    }
}

void heater_process(void){
    heater_calculate();
    heater_set_output();
}

void heater_calculate(void){

    // Delta is temperature we need to cool as positive value
    // Sensor cooling
    float delta_sensor = -1 * (uplink_out_message.setting.set_temp_sensor - uplink_out_message.system.temp_sensor);

    if(delta_sensor > HEATER_PELTIER_1_MAP_UPPER){
        uplink_out_message.state.percentage_peltier_1 = 100;
        uplink_out_message.state.sensor_state = Heating_State_HEATING_STATE_COOLING;
    } else if(delta_sensor < 0 ){
        uplink_out_message.state.percentage_peltier_1 = 0;
        uplink_out_message.state.sensor_state = Heating_State_HEATING_STATE_WARMING;
    } else {
        uplink_out_message.state.percentage_peltier_1 = map(delta_sensor, 0, HEATER_PELTIER_1_MAP_UPPER, 0, 100);
        uplink_out_message.state.sensor_state = Heating_State_HEATING_STATE_LEVELING;
    }

    // Peltier cooling
    float delta_peltier = -1 * ((uplink_out_message.setting.set_temp_sensor + HEATER_PELTIER_2_SET_TEMP_DELTA) - uplink_out_message.system.temp_peltier);

    if(delta_peltier > HEATER_PELTIER_2_MAP_UPPER){
        uplink_out_message.state.percentage_peltier_2 = 100;
    } else if(delta_peltier < 0 ){
        uplink_out_message.state.percentage_peltier_2 = 0;
    } else {
        uplink_out_message.state.percentage_peltier_2 = map(delta_peltier, 0, HEATER_PELTIER_2_MAP_UPPER, 0, 100);
    }

    // Cooling fan
    if (uplink_out_message.system.temp_cooling > HEATER_FAN_SET_TEMP){
        uplink_out_message.state.percentage_cooling_fan = 100;
    } else {
        uplink_out_message.state.percentage_cooling_fan = 0;
    }

    // telescope
    float delta_telescope = uplink_out_message.setting.set_temp_telescope - uplink_out_message.system.temp_telescope;

    if(delta_telescope > HEATER_TELESCOPE_MAP_UPPER){
        uplink_out_message.state.percentage_telescope = 100;
    } else if(delta_telescope < 0 ){
        uplink_out_message.state.percentage_telescope = 0;
    } else {
        uplink_out_message.state.percentage_telescope = map(delta_telescope, 0, HEATER_TELESCOPE_MAP_UPPER, 0, 100);
    }

    // guidance
    float delta_guidance = uplink_out_message.setting.set_temp_guidance - uplink_out_message.system.temp_guidance;

    if(delta_guidance > HEATER_GUIDANCE_MAP_UPPER){
        uplink_out_message.state.percentage_guidance = 100;
    } else if(delta_guidance < 0 ){
        uplink_out_message.state.percentage_guidance = 0;
    } else {
        uplink_out_message.state.percentage_guidance = map(delta_guidance, 0, HEATER_GUIDANCE_MAP_UPPER, 0, 100);
    }
}

void heater_set_output(void){
    analogWrite(PIN_OUT_PELTIER_1, map(uplink_out_message.state.percentage_peltier_1,   0, 100, 0, HEATER_PWM_MAX));
    analogWrite(PIN_OUT_PELTIER_2, map(uplink_out_message.state.percentage_peltier_2,   0, 100, 0, HEATER_PWM_MAX));
    analogWrite(PIN_OUT_FAN,       map(uplink_out_message.state.percentage_cooling_fan, 0, 100, 0, HEATER_PWM_MAX));
    analogWrite(PIN_OUT_TELESCOPE, map(uplink_out_message.state.percentage_telescope,   0, 100, 0, HEATER_PWM_MAX));
    analogWrite(PIN_OUT_GUIDANCE,  map(uplink_out_message.state.percentage_guidance,    0, 100, 0, HEATER_PWM_MAX));
}
