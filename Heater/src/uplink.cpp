#include <Arduino.h>

#include "pb_encode.h"
#include "pb_decode.h"
#include "oathat.pb.h"

#include "main.h"
#include "uplink.h"
#include "sensor.h"
#include "heater.h"

// Public Variables
Heater2Hat uplink_out_message = Heater2Hat_init_zero;

// Local Variables
uint8_t        receive_buffer[UPLINK_BUFFER_SIZE] = {0};
uint16_t       receive_buffer_index               = 0;
uint16_t       receive_size                       = 0;
uint8_t        send_buffer   [UPLINK_BUFFER_SIZE] = {0};
uint16_t       send_buffer_index                  = 0;
uint16_t       send_size                          = 0;
receive_states receive_buffer_state               = WAITING_FOR_E;
// Private Functions


void uplink_init(void){
  Serial.begin(115200);
  uplink_out_message.has_setting                = true;
  uplink_out_message.setting.set_temp_guidance  = 0;
  uplink_out_message.setting.set_temp_telescope = 0;
  uplink_out_message.setting.set_temp_sensor    = 40;
}

void uplink_send(void){
  if (send_size == 0) {
    memcpy(send_buffer, "ELON", 4);
    pb_ostream_t ostream = pb_ostream_from_buffer(&send_buffer[6], sizeof(send_buffer) - 6);
    pb_encode(&ostream, &Heater2Hat_msg, &uplink_out_message);
    send_buffer[4] = (ostream.bytes_written >> 8) & 0xFF;
    send_buffer[5] = (ostream.bytes_written >> 0) & 0xFF;
    send_size      = 6 + ostream.bytes_written;
  }
}

void uplink_process(void){
  while (Serial.available() > 0){
    uint8_t data = Serial.read();

    if (receive_buffer_state == WAITING_FOR_E){
      if (data == 0x45){
        receive_buffer_state = WAITING_FOR_L;
      } else {
        receive_buffer_state = WAITING_FOR_E;
      }
      continue;
    }

    if (receive_buffer_state == WAITING_FOR_L){
      if (data == 0x4c){
        receive_buffer_state = WAITING_FOR_O;
      } else {
        receive_buffer_state = WAITING_FOR_E;
      }
      continue;
    }

    if (receive_buffer_state == WAITING_FOR_O){
      if (data == 0x4f){
        receive_buffer_state = WAITING_FOR_N;
      } else {
        receive_buffer_state = WAITING_FOR_E;
      }
      continue;
    }

    if (receive_buffer_state == WAITING_FOR_N){
      if (data == 0x4e){
        receive_buffer_state = WAITING_FOR_SIZE_H;
      } else {
        receive_buffer_state = WAITING_FOR_E;
      }
      continue;
    }

    if (receive_buffer_state == WAITING_FOR_SIZE_H){
      receive_size = (uint16_t)data << 8;
      receive_buffer_state = WAITING_FOR_SIZE_L;
      continue;
    }

    if (receive_buffer_state == WAITING_FOR_SIZE_L){
      receive_size += data;
      receive_buffer_state = WAITING_FOR_DATA;
      continue;
    }

    if (receive_buffer_state == WAITING_FOR_DATA){
      receive_buffer[receive_buffer_index++] = data;
    }

    if (receive_buffer_index >= receive_size){
      pb_istream_t istream;
      istream = pb_istream_from_buffer(receive_buffer, receive_size);
      Hat2Heater decoded = Hat2Heater_init_zero;

      if(pb_decode(&istream, &Hat2Heater_msg, &decoded)){
          uplink_out_message.setting.set_temp_sensor    = decoded.setting.set_temp_sensor;
          uplink_out_message.setting.set_temp_telescope = decoded.setting.set_temp_telescope;
          uplink_out_message.setting.set_temp_guidance  = decoded.setting.set_temp_guidance;
      }

      receive_buffer_state = WAITING_FOR_E;
      receive_buffer_index = 0;
    }
  }

  if(send_size > 0){
    Serial.write(send_buffer[send_buffer_index++]);

    if (send_buffer_index >= send_size){
      send_buffer_index = 0;
      send_size         = 0;
    }
  }
}

////////////////////
// Private functions
////////////////////
