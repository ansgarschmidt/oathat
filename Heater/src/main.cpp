#include <Arduino.h>

#include "main.h"
#include "sensor.h"
#include "uplink.h"
#include "heater.h"
#include "ux.h"

uint16_t inhibitor_send = 0;

void setup() {
  uplink_init();
  sensor_init();
  heater_init();
  ux_init();
  uplink_out_message.has_state             = true;
  uplink_out_message.has_system            = true;
  uplink_out_message.has_environment       = true;
  uplink_out_message.has_setting           = true;
  uplink_out_message.system.version_heater = HEATER_VERSION;
}

void loop() {
  sensor_measure();
  uplink_process();
  heater_process();
  ux_process();
  heater_cooling_ramp();

  if (inhibitor_send++ > MAIN_SEND_DELAY){
      uplink_send();
  }
}
