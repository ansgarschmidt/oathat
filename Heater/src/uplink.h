#pragma once

#include "oathat.pb.h"

#define UPLINK_BUFFER_BUFFER  20
#define UPLINK_BUFFER_SIZE    (Heater2Hat_size + UPLINK_BUFFER_BUFFER)

extern Heater2Hat uplink_out_message;

typedef enum _reveice_states{
     WAITING_FOR_E,
     WAITING_FOR_L,
     WAITING_FOR_O,
     WAITING_FOR_N,
     WAITING_FOR_SIZE_H,
     WAITING_FOR_SIZE_L,
     WAITING_FOR_DATA
} receive_states;


void uplink_init   (void);
void uplink_send   (void);
void uplink_process(void);