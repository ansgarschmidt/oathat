#include <Arduino.h>

#include "main.h"
#include "ux.h"
#include "pinout.h"
#include "uplink.h"
#include "heater.h"

volatile bool     buttonFlag    = false;
         uint32_t previousPress = 0;

void ux_button_ISR    (void);

void ux_init(void){
    pinMode     (PIN_IN_BUTTON,     INPUT);
    attachInterrupt(digitalPinToInterrupt(PIN_IN_BUTTON), ux_button_ISR, CHANGE);

    pinMode(PIN_OUT_STATUS_1,  OUTPUT);
    pinMode(PIN_OUT_STATUS_2,  OUTPUT);
    pinMode(PIN_OUT_STATUS_3,  OUTPUT);

    digitalWrite(PIN_OUT_STATUS_1,  HIGH );
    digitalWrite(PIN_OUT_STATUS_2,  HIGH );
    digitalWrite(PIN_OUT_STATUS_3,  HIGH );
}

void ux_process(void){
    if((millis() - previousPress) > UX_BUTTON_DEBOUNCE && buttonFlag){
        previousPress = millis();
        heater_start_ramp();
    }
    buttonFlag = false;

    if (uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_OFF){
        digitalWrite(PIN_OUT_STATUS_1,  HIGH );
        digitalWrite(PIN_OUT_STATUS_2,  HIGH );
        digitalWrite(PIN_OUT_STATUS_3,  HIGH );
    }

    if (uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_COOLING){
        digitalWrite(PIN_OUT_STATUS_1,  LOW );
        digitalWrite(PIN_OUT_STATUS_2,  HIGH );
        digitalWrite(PIN_OUT_STATUS_3,  HIGH );
    }

    if (uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_LEVELING){
        digitalWrite(PIN_OUT_STATUS_1,  HIGH );
        digitalWrite(PIN_OUT_STATUS_2,  LOW );
        digitalWrite(PIN_OUT_STATUS_3,  HIGH );
    }

    if (uplink_out_message.state.sensor_state == Heating_State_HEATING_STATE_WARMING){
        digitalWrite(PIN_OUT_STATUS_1,  HIGH );
        digitalWrite(PIN_OUT_STATUS_2,  HIGH );
        digitalWrite(PIN_OUT_STATUS_3,  LOW );
    }
}

void ux_button_ISR(void){
    buttonFlag = true;
}
