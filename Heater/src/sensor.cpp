#include <Arduino.h>

#include <Thermistor.h>
#include <NTC_Thermistor.h>
#include "sensor.h"
#include "uplink.h"
#include "pinout.h"

// Thermistors
Thermistor* thermistor_sensor;
Thermistor* thermistor_peltier;
Thermistor* thermistor_cooling;
Thermistor* thermistor_air;
Thermistor* thermistor_telescope;
Thermistor* thermistor_guidance;

float sensor    = 0;
float peltier   = 0;
float cooling   = 0;
float telescope = 0;
float guidance  = 0;
float air       = 0;

void sensor_init(void){                     //PIN               R0     Rn      Tn  BB
    thermistor_sensor    = new NTC_Thermistor(PIN_IN_SENSOR,    81100, 100000, 25, 3950);
    thermistor_peltier   = new NTC_Thermistor(PIN_IN_PELTIER,   81100, 100000, 25, 3950);
    thermistor_cooling   = new NTC_Thermistor(PIN_IN_COOLING,   81100, 100000, 25, 3950);
    thermistor_air       = new NTC_Thermistor(PIN_IN_AIR,       81100, 100000, 25, 3950);
    thermistor_telescope = new NTC_Thermistor(PIN_IN_TELESCOPE, 81100, 100000, 25, 3950);
    thermistor_guidance  = new NTC_Thermistor(PIN_IN_GUIDANCE,  81100, 100000, 25, 3950);

    sensor    = thermistor_sensor->readCelsius();;
    peltier   = thermistor_peltier->readCelsius();
    cooling   = thermistor_cooling->readCelsius();
    telescope = thermistor_telescope->readCelsius();
    guidance  = thermistor_guidance->readCelsius();
    air       = thermistor_air->readCelsius();
}

void sensor_measure(void){
  uplink_out_message.system.temp_sensor             = (SENSOR_LOW_PASS_FILTER_OLD * sensor   ) + (SENSOR_LOW_PASS_FILTER_NEW * thermistor_sensor->readCelsius()   );
  uplink_out_message.system.temp_peltier            = (SENSOR_LOW_PASS_FILTER_OLD * peltier  ) + (SENSOR_LOW_PASS_FILTER_NEW * thermistor_peltier->readCelsius()  );
  uplink_out_message.system.temp_cooling            = (SENSOR_LOW_PASS_FILTER_OLD * cooling  ) + (SENSOR_LOW_PASS_FILTER_NEW * thermistor_cooling->readCelsius()  );
  uplink_out_message.system.temp_telescope          = (SENSOR_LOW_PASS_FILTER_OLD * telescope) + (SENSOR_LOW_PASS_FILTER_NEW * thermistor_telescope->readCelsius());
  uplink_out_message.system.temp_guidance           = (SENSOR_LOW_PASS_FILTER_OLD * guidance ) + (SENSOR_LOW_PASS_FILTER_NEW * thermistor_guidance->readCelsius() );
  uplink_out_message.environment.temp_air_telescope = (SENSOR_LOW_PASS_FILTER_OLD * air      ) + (SENSOR_LOW_PASS_FILTER_NEW * thermistor_air->readCelsius()      );
}
