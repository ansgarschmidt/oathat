#pragma once

#define HEATER_WAIT_TIME_BETWEEN_STEP (1000 * 23)
#define HEATER_RAMP_LOW                -30
#define HEATER_RAMP_HIGH                30
#define HEATER_PELTIER_1_MAP_UPPER      20
#define HEATER_PELTIER_2_MAP_UPPER      20
#define HEATER_PELTIER_2_SET_TEMP_DELTA 20
#define HEATER_FAN_SET_TEMP             30
#define HEATER_TELESCOPE_MAP_UPPER      20
#define HEATER_GUIDANCE_MAP_UPPER       20
#define HEATER_PWM_MAX                 255
#define HEATER_TEMP_DELTA_HEATING        5

void heater_init        (void);
void heater_process     (void);
void heater_cooling_ramp(void);
void heater_start_ramp  (void);
