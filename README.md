# OpenAstroTracker Raspberry PI Hat
This projects adds hardware and support firmware to the (OpenAstroTracker)[https://openastrotech.com] project. It replaces the RAMPS hardware with a custom made system which contains all of the hardware features for the (OpenAstroTracker-Firmware)[https://github.com/OpenAstroTech/OpenAstroTracker-Firmware] and additionally more functionality which shows to be usefull when doing long outside observations. 

## Features

### Related to the OpenAstroTracker-Firmware

### Additional Features in Hardware
* GPS is shared among the OpenAstroTracker Firmware and the GPSD on the Raspi
* RTC for Raspberyy Pi with ds3231
* Battery management for 12 V power
* Shutdown IO PIN
* IR temperature sensor for cloud coverage detection via MLX90614
* Light sensor GY-302
* White and Red Lights 
* Survailance (when controlled remotely from e.g. camper)
    * 4 PIR sensors for movement detection
    * Turnable IR Camera
* Controll for heatmats to avoid condense on the telescope / guidance camera
* Peltier cooling for the camera sensor 

### Additinal Features in Software for the Raspberry Pi
* Simple ux to see sensor values and set parameters
* All sensor data is exposed via Prometheus -> Grafana
* Script to add sensor information to the generated FIT Files 
* OpenAstroTracker-Firmware update via direct ICSP connection
* INDI driver to expose sensors and control heater / cooler





 ## System
 ![](Doc/OAT-HAT.png)

The system consists of 3 hardware parts
1. The Raspberry pi HAT 
   with all stepper drivers, RTC, light controlls and connectors for survailance hardware. This one can be stored savely under or next to the OAT
2. The Heater / Cooler
   to controll the peltier / heatmat directly next to the camera, so there is only need for a small UART and 12V Power cable to keep the setup clean.
3. GPS / Sensor
   containing the GPS module, level sensor, environmental sensors which needs to be exposed. Also on a small breakout board