#include <Arduino.h>
#include <Thermistor.h>
#include <NTC_Thermistor.h>

Thermistor* thermistor;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

  thermistor = new NTC_Thermistor(
    A0,
    81100,
    100000,
    25,
    3950
  );

}

void loop() {
  const double celsius    = thermistor->readCelsius();
  const double kelvin     = thermistor->readKelvin();
  const double fahrenheit = thermistor->readFahrenheit();

  // Output of information
  Serial.print("Temperature: ");
  Serial.print(celsius);
  Serial.print(" C, ");
  Serial.print(kelvin);
  Serial.print(" K, ");
  Serial.print(fahrenheit);
  Serial.println(" F");

  delay(500); // optionally, only to delay the output of information in the example.
}
